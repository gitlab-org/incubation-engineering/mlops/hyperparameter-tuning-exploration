# Exploring HyperParameter Tuning on GitLab

[Hyperparameter Tuning (or optmization)](https://en.wikipedia.org/wiki/Hyperparameter_optimization) 
is the process in ML development in which  we optimize the parameters passed to the algorithm used 
on the model.

Here we are exploring on strategies to automate it within GitLab.

**Structure**

This exploration is divided in parts that get progressively more complex. The code for each part
is available on a branch of this repository. This Readme will keep a summary of the results, 
which is also available on the epic for this exploration https://gitlab.com/groups/gitlab-org/incubation-engineering/mlops/-/epics/6.

# Part 0: What is [Hyperparameter Optimization](https://en.wikipedia.org/wiki/Hyperparameter_optimization)

It is the process of automatically choosing the configuration parameters that yield the best model given and algorithm and a dataset. This is a resource and time intensive task, required the training of hundreds of models, but that can greatly impact product performance. We are exploring here what does it take to use GitLab Pipelines to perform this task.

[![](http://img.youtube.com/vi/axi9a4LMaHI/0.jpg)](http://www.youtube.com/watch?v=axi9a4LMaHI "Part 0: What is Hyperparameter Optimization")

[Video](https://youtu.be/axi9a4LMaHI) | [Slides](https://docs.google.com/presentation/d/19zOQa2_hIpZybfgW7K66T2dQYWsrKxGN8W5Uu-NOE8M/edit?usp=sharing) | [Code](https://gitlab.com/gitlab-org/incubation-engineering/mlops/hyperparameter-tuning-exploration/-/blob/main/notebooks/hyperparameter_tuning.ipynb) 

More Resources: 
- https://wandb.ai/site/articles/running-hyperparameter-sweeps-to-pick-the-best-model-using-w-b
- https://cloud.google.com/blog/products/ai-machine-learning/hyperparameter-tuning-using-tpus-cloud-ml-engine

# Part 1: The Simplest Hypertparameter Optimization Pipeline

> Key Takeaway: The `change -> commit -> test -> check -> change` iteration cycle is very slow, and while online tools like the Pipeline Editor and the Validation in [GitLab Workflow](https://marketplace.visualstudio.com/items?itemName=GitLab.gitlab-workflow) do help, testing locally would be a huge boost in productivity


[![](http://img.youtube.com/vi/ju4s7IPDAWk/0.jpg)](http://www.youtube.com/watch?v=ju4s7IPDAWk "Part 1: The Simplest Hyperparameter Optimization Pipeline")

[Video](http://www.youtube.com/watch?v=ju4s7IPDAWk) | [Slides](https://docs.google.com/presentation/d/19zOQa2_hIpZybfgW7K66T2dQYWsrKxGN8W5Uu-NOE8M/edit?usp=sharing) |
[Code](https://gitlab.com/gitlab-org/incubation-engineering/mlops/hyperparameter-tuning-exploration/-/tree/part_1_the_simplest_hyperopt_pipeline)

# Part 2: Running the Trials in Parallel

Running Hyperparameter Optimization sequentially becomes quickly impractical as the size of data
and model complexity increases. To parallelize this process, we make use of Dynamic Parent-Child 
pipelines to build a CI pipeline where each trial is generated during the run of the pipeline.

> Recording Soon

Improvement Points:

- Iterating is slow, very slow. When coding the sequential pipeline, we could still use
the Pipeline Editor, but since we are relying on a dynamic pipeline, that is not possible.
What I did do was to first create a child pipeline, with some hardcoded run trials, test
if everything was ok, and then copy to the template, connecting the dots later

- Allow jobs to depend on a stage as well. This would make configuration a lot easier to
parse, and we already have an issue for that (gitlab-org/gitlab#220758)

- On the iteration, one of the things that makes it specially slow is that although you
can test if the scripts are working fine, testing the CI config in itself is slow,
specially if the bug is by the end of the pipeline, requiring one to repeat all steps
again. A checkpoint feature would be extremelly useful for speeding up time: save the
state of a previous run up until a stage/step and only replay what comes after that.

# Part 3: Iterative Search

> SOON

# Part 4: From Jupyter to GitLab Pipelines

> SOON